import shell from 'shelljs'
import PouchDB from 'pouchdb'
import express from 'express'
import chalk from 'chalk';

const argv = require('yargs')
  .scriptName('netwatcher')
  .usage('Log and monitor your ping latency\nUsage: $0 <ping arguments>')
  .options({
    'i': {
      alias: 'interval',
      default: '5',
      describe: 'interval in seconds to wait before successive pings',
      type: 'number'
    },
    't': {
      alias: 'target',
      default: 'google.com',
      describe: 'target host to ping',
      type: 'string'
    }
  })
  .argv

const db = new PouchDB('ping-history')

const web = express()
web.use('static', express.static('client/static'))
web.get('/', (req, res) => res.send('Hello'));
const service = web.listen(
  5565,
  () => console.log('Hosting monitoring results at http://localhost:5565'))

console.log('Net gathers, and now my watch begins. It shall not end until my ^C')
process.on('SIGINT', terminate)

const pingProcess = shell.exec(`ping -i ${argv.i} ${argv.t}`, {async: true, silent: true})
pingProcess.on('exit', (code) => {
  if (code != null && code > 0) {
    console.log(`My watch has ended, with code ${code}`)
    process.exit(code)
  }
})
pingProcess.stdout!.on('data', (data) => {
  if (!data) {
    console.error('Empty ping result')
    return
  }
  if (data.includes('ping statistics')) {
    // It's just a final summary while quitting. Ignore it.
    return
  }
  addEntry(data)
})

function addEntry(pingOut: string) {
  if (pingOut.toLowerCase().includes('unreachable') || pingOut.toLowerCase().includes('timed out')) {
    db.post({
      'time': Date.now(),
      'unreachable': true
    })
    return
  }
  const pingExtractRegex = /time=(?<time>\d+\.\d+)/
  const match = pingOut.match(pingExtractRegex)
  if (!/\S/.test(pingOut)) {
    // It's just whitespace. Don't do anything.
    return
  }
  if (!match?.groups?.time) {
    console.error(`Ping result cannot be parsed:\n  ${pingOut}`)
    return
  }

  const latency = Number(match!.groups!.time)

  db.post({
    'time': Date.now(),
    'latency': latency
  })

  let progressBlock = ''
  let coloringFunction: chalk.Chalk
  if (latency < 10) {
    coloringFunction = chalk.green
    progressBlock = '▁'
  } else if (latency < 20) {
    coloringFunction = chalk.green
    progressBlock = '▂'
  } else if (latency < 30) {
    coloringFunction = chalk.green
    progressBlock = '▃'
  } else if (latency < 40) {
    coloringFunction = chalk.yellow
    progressBlock = '▄'
  } else if (latency < 50) {
    coloringFunction = chalk.yellow
    progressBlock = '▅'
  } else if (latency < 100) {
    coloringFunction = chalk.red
    progressBlock = '▆'
  } else if (latency < 200) {
    coloringFunction = chalk.red
    progressBlock = '▇'
  } else {
    coloringFunction = chalk.red
    progressBlock = '█'
  }
  process.stdout.write(coloringFunction(progressBlock))
}

async function terminate() {
  console.log('\n') // ^C doesn't leave a new line.
  const info = await db.info()
  const last = await db.changes({
    limit: 1,
    include_docs: true
  })
  console.log(`Database has ${info.doc_count} entries since ` +
              `${new Date((last.results[0].doc as any).time).toDateString()}.`)
  console.log('My watch has ended')
  service.close()
  process.exit()
}